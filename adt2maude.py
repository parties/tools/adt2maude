#!/usr/bin/env python
"""
A parser from ADTree specifications to Maude.

Main definition:  file2system(filename) -> str:

Expected File Format:
    MainAttack

    id1 type1 side1 time1 cost1
    ...
    idn typen siden timen costn

    idx1 child1 ... childm
    ...
    idxk child1 ... childm

"""

from dataclasses import dataclass
from enum import Enum, unique


@unique
class SIDE(Enum):
    ATTACK = "attack"
    DEFENSE = "defence"


@unique
class ATKType(Enum):
    LEAF = "leaf"
    AND = "and"
    OR = "or"
    SAND = "sand"
    NAND = "nand"
    CAND = "cand"
    SNAND = "snand"
    NOR = "nor"


@dataclass
class ADTAttack:
    """Representation for attacks and defenses"""

    label: str
    side: SIDE
    atktype: ATKType
    time: int
    cost: int
    children: list[str]


@dataclass
class ADTree:
    """ADT representation"""

    # Main attack
    main: str
    # From IDs to ADTNodes objects
    attacks: dict

    def __init__(self, main: str):
        self.main = main
        self.attacks = {}


def file2adtree(filename) -> ADTree:
    """Parsing the file into an ADTree"""

    with open(filename, "r") as fhand:
        vlist = [line.rstrip() for line in fhand]

        ADT = ADTree(vlist[0])

        # First part (before \n) with Attack definitions
        for idx, v in enumerate(vlist[2:]):
            if v == "":
                break

            [vid, vtype, vside, vcost, vtime] = v.split()
            ADT.attacks[vid] = ADTAttack(
                label=vid,
                side=SIDE(vside),
                atktype=ATKType(vtype),
                time=vtime,
                cost=vcost,
                children=[],
            )

        # Second part: the children of each node
        for v in vlist[idx + 3 :]:
            lval = v.split()
            ADT.attacks[lval[0]].children = lval[1:]

        return ADT


def adt2maude(ADT: ADTree) -> str:
    """Parsings the ADTree structure into a Maude specification"""
    s = f"{{ '{ADT.main} ;\n"

    for A in ADT.attacks.values():
        match A.atktype:
            case ATKType.LEAF:
                if A.side == SIDE.ATTACK:
                    s += f"\tmakeAtk('{A.label}, {A.time}, {A.cost})\n"
                else:
                    s += f"\tmakeDef('{A.label}, {A.time}, {A.cost})\n"
            case ATKType.AND:
                ch = ",".join([f"'{x}" for x in A.children])
                s += f"\tmakeAnd('{A.label}, ({ch}) , {A.time}, {A.cost})\n"
            case ATKType.NAND | ATKType.CAND:
                ch = ",".join([f"'{x}" for x in A.children[:-1]])
                neg = f"{A.children[-1]}"
                nch = f"\tmakeNot('~{neg},'{neg})\n"
                s += f"\tmakeAnd('{A.label}, ({ch},'~{neg}) , {A.time}, {A.cost})\n"
                s += nch
            case ATKType.SNAND:
                ch = " ".join([f"'{x}" for x in A.children[:-1]])
                neg = f"{A.children[-1]}"
                nch = f"\tmakeNot('~{neg},'{neg})\n"
                s += f"\tmakeSAnd('{A.label}, ({ch} '~{neg}) , {A.time}, {A.cost})\n"
                s += nch
            case ATKType.SAND:
                ch = " ".join([f"'{x}" for x in A.children])
                s += f"\tmakeSAnd('{A.label}, ({ch}) , {A.time}, {A.cost})\n"
            case ATKType.OR:
                ch = ",".join([f"'{x}" for x in A.children])
                s += f"\tmakeOr('{A.label}, ({ch}) , {A.time}, {A.cost})\n"
            case ATKType.NOR:
                ch = ",".join([f"'{x}" for x in A.children[:-1]])
                neg = f"{A.children[-1]}"
                nch = f"\tmakeNot('~{neg},{neg})\n"
                s += f"\tmakeOr('{A.label}, ({ch},~{neg}) , {A.time}, {A.cost})\n"
                s += nch

    return s + " } "


def file2system(filename) -> str:
    """From files to Maude specification"""
    return adt2maude(file2adtree(filename))
