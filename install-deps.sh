#!/bin/bash

###############################################################################
#         FILE: install-deps.sh
#        USAGE: ./install-deps.sh
#  DESCRIPTION: Script to install all the dependencies of adt2maude
#       AUTHOR: Jaime Arias <arias@lipn.univ-paris13.fr>
###############################################################################

deps=( "python3" "pip3" "wget" "unzip" )

# check operating system
os=$(uname -s)
if [ "$os" != "Linux" ] && [ "$os" != "Darwin" ]; then
  echo "$os is not supported by this script"
  exit 1
fi

# check dependencies
for i in "${deps[@]}"; do
  if ! command -v $i &> /dev/null ; then
      echo "$i is not installed"
      exit 1
  fi
done

# check the correct version of python
python_version=$(python3 -c"import sys; print(sys.version_info.minor)")
if [ "$python_version" -lt "10" ]; then
  echo "python 3.10 or greater is not installed"
  exit 1
fi

# installing requirements
echo "Installing python dependencies ..."
pip3 install -q -r requirements.txt

# installing maude
if [ "$os" = "Darwin" ]; then
     echo 'Downloading Maude for OSX ...'
     wget -c https://github.com/SRI-CSL/Maude/releases/download/3.2.1/Maude-3.2.1-macos.zip -O maude.zip -q
else
     echo 'Downloading Maude for Linux ...'
     wget -c https://github.com/SRI-CSL/Maude/releases/download/3.2.1/Maude-3.2.1-linux.zip -O maude.zip -q
fi

# unzip maude folder and clean
unzip -q maude.zip
mv "${os}64" maude
rm maude.zip

echo -e "\nAdd maude to your PATH."
echo "export PATH=\"${PWD}/maude"':$PATH"'