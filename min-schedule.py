#!/usr/bin/env python

"""
Finding the minimal schedule by iterating on the number of agents.

The procedure is as follows:
     1. The ADT file (txt) is translated into a Maude specification (see
     ast2maude.py)

     2. The semantics defined in semantics.maude is used to compute the minimal
     time for the attack and also an upper bound for the number of agents
     needed.  Several solutions are possible (with different defenses).

     3. For each configuration, the specification in semantics-schedule.maude
     is used to find the schedule (assigning agents to attacks). This script
     iterates with a number of agents from 1 to the upper bound and stops when
     a schedule is found.

This script requires Maude 3.2 installed (and available from the current PATH)
It also requires the Maude bindings for python (available at
https://pypi.org/project/maude/)
"""

import argparse
import os
import pathlib
import re
import traceback

from prettytable import PrettyTable

import maude
from adt2maude import file2system

# set absolute path to load the python script
PATH = pathlib.Path(os.path.abspath(__file__)).parent


def escape_ansi(line):
    ansi_escape = re.compile(r"(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]")
    return ansi_escape.sub("", str(line))


def findMinimalTime(filename: str, printtable: bool):
    """Finding the minimal time of the attack"""

    # 2. Step (using semantics.maude)
    maude.load(os.path.join(PATH, "semantics.maude"))
    m = maude.getModule("SEMANTICS-STR")
    system = file2system(filename)
    # print(system)
    termS = m.parseTerm(system)
    termGoal = m.parseTerm("{ C:Configuration }")
    # Using the strategy solve
    strategySolve = m.parseStrategy("solve")
    res = termS.srewrite(strategySolve)

    # Dictionary from defenses to tuples (time, list[tuple[attack, numagents]])
    # The second component is a list since, for the same time, it is possible to
    # have different configurations.
    configurations = {}

    for result in res:
        # Getting the needed information from the term
        term = result[0]
        strTerm = escape_ansi(str(term))
        if "gates: " not in strTerm:
            continue

        attacks = re.search("(gates: )([^>]*)", strTerm).group(2)
        defenses = re.search("(defenses: )([^>]*)", strTerm).group(2)
        agents = int(re.search("(agents: )([^,]*)", strTerm).group(2))
        time = int(re.search("(acctime: )([^,]*)", strTerm).group(2))

        # Adding the new configuration (comparing the minimal time)
        if defenses in configurations:
            if time < configurations[defenses][0]:
                configurations[defenses] = (time, [(attacks, agents)])
            elif time == configurations[defenses][0]:
                configurations[defenses][1].append((attacks, agents))
        else:
            configurations[defenses] = (time, [(attacks, agents)])

    # 3. step (using semantics-schedule)
    maude.load(os.path.join(PATH, "semantics-schedule.maude"))
    m = maude.getModule("SEMANTICS")

    # Set of tuples <time, attacks> (to avoid computing the same configuration
    # twice)
    answers = set()

    # Iterating on the configuration found
    for defenses, value in configurations.items():
        time, LAttacks = value

        # For each sub-configuration
        for attacks, agents in LAttacks:
            if (time, attacks) in answers:
                break

            answers.add((time, attacks))
            found = False

            # Iterating on the number of agents
            for i in range(1, agents + 1):
                if found:
                    break
                # print(f'Trying with {i} agent(s)')

                termS = m.parseTerm(
                    f"make-schedule({i}, {time}, ({attacks}), ({system}))"
                )
                termGoal = m.parseTerm("{ agents: SL:ScheduleList global-time: n:Nat} ")
                res = termS.search(1, termGoal)

                # Checking the solutions from the search command
                for result in res:
                    term = result[0]
                    # schedule = re.search("(agents: )(.*)(global*)", str(term)).group(2)
                    print(f"file: {filename} time: {time} agents: {i}")
                    if printtable:
                        tab = PrettyTable(["slot"] + [f"A{i}" for i in range(1, i + 1)])
                        tab.add_rows(listSchedule(result[2]()))
                        print(tab)
                    found = True
                    break  # only one solution is needed


def listSchedule(steps):
    """steps is a list term and rule (see pathTo in Maude's documentation)"""

    # terms before/after
    term = None, None
    btime = False
    slot = 1
    result = []
    for st in steps:
        if type(st) == maude.Rule:
            if st.getLabel() == "time":
                btime = True
        else:
            term = term[1], st
            strTerm = escape_ansi(str(term[0]))
            if btime:
                agents = re.search("(agents: )((.*)*\n)", strTerm).group(2)
                ltask = tasks(agents)
                result.append([slot] + ltask)
                slot += 1
                btime = False

    return result


def tasks(s: str):
    """Returning a list with the task currently being executed"""

    # s is expected to be of the form ['f 'ST 'h]:: 2) (['b]:: 0)
    l = []
    for sch in re.finditer(r"(\[)(.*?)(\]:: )(.*?)(\)|\n)", s):
        task = sch.group(2).replace("'", "").split()
        task_time = int(sch.group(4))
        if task_time > 0 and task:
            l.append(task[-1])
        else:
            l.append("-")
    return l


def main(fin, table):
    """File name and option for printing the scheduling table"""
    try:
        # stime = time.time()
        print(f"Processing {fin}.")
        maude.init(advise=False)
        findMinimalTime(fin, table)
        # print(f'Elapsed time: {time.time() - stime}')
    except Exception as E:
        print(f"Error processing {fin}.\n {E}")
        traceback.print_exc()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=""" From ADTrees to Maude specifications """,
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument("--input", type=str, required=True, help="ADTree file")

    parser.add_argument(
        "--table",
        type=bool,
        default=False,
        help="Print table (restricted due to Maude binding implementation)",
        action=argparse.BooleanOptionalAction,
    )

    args = parser.parse_args()
    main(args.input, args.table)
